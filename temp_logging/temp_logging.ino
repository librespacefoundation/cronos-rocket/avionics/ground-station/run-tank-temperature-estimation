#include <OneWire.h>
#include <DallasTemperature.h>
#include <SPI.h>
#include <SD.h>
#include <TimeLib.h>


// All DS18B20 Sensors are connected to pin 8 on the Arduino
#define ONE_WIRE_BUS 8
#define FILE_WRITE O_WRITE | O_CREAT | O_APPEND

File myFile;

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

unsigned long myTime;
int state = -1;
bool voltage_level = false;


int numberOfDevices; //To store number of sensors connected
DeviceAddress tempDeviceAddress; // Variable to store a single sensor address

// function to print a sensor address
void printAddress(DeviceAddress deviceAddress);
void printTemp();


void setup(void) {
  Serial.begin(9600);

  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.print("Initializing SD card...");

  if (!SD.begin(10)) {
    Serial.println("initialization failed!");
    while (1);
  }

  Serial.println("initialization done.");

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  sensors.begin();
  // Get the number of sensors connected to the wire( digital pin 4)
  numberOfDevices = sensors.getDeviceCount();
  Serial.print(numberOfDevices, DEC);
  Serial.println(" devices.");

  // Loop through each sensor and print out address
  for (int i = 0; i < numberOfDevices; i++) {
    // Search the data wire for address and store the address in "tempDeviceAddress" variable
    if (sensors.getAddress(tempDeviceAddress, i)) {
      Serial.print("Found device ");
      Serial.print(i, DEC);
      Serial.print(" with address: ");
      sensors.setResolution(tempDeviceAddress, 9);
      printAddress(tempDeviceAddress);
      Serial.println();
    } else {
      Serial.print("Found ghost device at ");
      Serial.print(i, DEC);
      Serial.print(" but could not detect address. Check power and cabling");
    }
  }

  printTemp();

  myFile.print("Time|");
  myFile.println("Sensor 0|Sensor 1|Sensor 2|Sensor 3|Sensor 4|State|");

  delay(5100);
  voltage_level = digitalRead(3);
}

void loop(void) {
  printTemp(); 
  }



// implementations

void printAddress(DeviceAddress deviceAddress) {
  for (uint8_t i = 0; i < 8; i++) {
    if (deviceAddress[i] < 16) {
      myFile.print("0");
      myFile.print(deviceAddress[i], DEC); //note: changed HEX to DEC
    }
  }
}

void printTemp() {
  myFile = SD.open("temp.txt", FILE_WRITE);
  sensors.requestTemperatures();
  myTime = millis();
  myFile.print(myTime); // prints time since program started
  //Serial.print(myTime);
  myFile.print("|");
  //Serial.print("|");


  // Send the command to get temperatures from all sensors.
  // Loop through each device, print out temperature one by one
  for (int i = 0; i < numberOfDevices; i++) {
    // Search the wire for address and store the address in tempDeviceAddress
    if (sensors.getAddress(tempDeviceAddress, i)) {
      // Print the temperature
      float tempC = sensors.getTempC(tempDeviceAddress); //Temperature in degree celsius
      myFile.print(tempC);
      //Serial.print(tempC); 
      myFile.print("|");
      //Serial.print("|"); 
    }
  }
  bool temp_level = digitalRead(3);
  if (temp_level != voltage_level) {
    state++;
    voltage_level = temp_level;
  }

  myFile.print(state);
  myFile.println();
  //Serial.println();
  myFile.close();
}
