# Description
Software for logging temperature values of the exterior of the Run Tank and saving them in an SD card. The Wiki for this Temperature Logging System can be found [here.](https://gitlab.com/groups/librespacefoundation/cronos-rocket/avionics/-/wikis/Temperature-Logging)

Necessary Libraries in order to run this code: 

1. OneWire

2. DallasTemperature

3. SPI

4. SD

5. TimeLib 

There should also be an empty "temp.txt" (logging) file in the SD card to avoid possible failure of code creating it automatically and in general data loss.
The system logs data successfully (sensor recognition, flushing first "bad" data) after approximately 1 minute. 

